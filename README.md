# KSON-radio-bot
Purpose:  Help win radio contests. The format is radio DJ calls a name out of pool at SPECIFIC times of the day.  
This de-coupled application will tune in at the specific time in which the winners are announced, transcribe to text, then send to your mobile phone via text message.

install, and unpackage in your home directory

# How it works: 
One timer:  Bootstrap EC2 instance using "Launch Template."  Builds out the centos7 server fully, pulls my repo with proper code/cronjobs. 

![](documentation/KSON-Bot.jpg)

1. CloudWatch alarm uses powers on instance at 7:30AM, then powers it off at 6PM daily. 2 simple lambda functions help achieve this.  
 
1. OS-Cronjob initiates a VLC recording of radio daily for 2 minutes at the following times:
`at 7:40am, 9:40am, 10:40 am, 12:40pm, 1:40pm, 2:40pm, 3:40pm, and 5:40pm`
 
1. The output of the recording is a 2 minute-long .mp3 file, and is saved locally in EC2 instance. Using AWS API, the file is copied frome EC2 to S3 bucket; EC2 is authorized thanks to S3 role.
 
1. EC2 Role provides EC2  permission to send transcription jobs to "Amazon transcribe" via API.  The .mp3 file is sent to S3 bucket for transcribe job. 
 
1. EC2 queries "amazon transcribe" to get the raw text resulting from the transcription job, then saves that output as a .txt file locally in EC2 instance. 
 
1. The .txt file is sent from EC2 instance, to an S3 bucket. 
 
1. A lambda function is triggers when a .txt suffix is uploaded to S3.  The triggered python lamda function will store the contentents of .txt file in a vairable. 
 
1. Lambda Function will send the variable to SNS subscriber phone number. 


The result? 
At specific times, a user recieves a text containing the winner of the Coachella Tickets.  If you're so lucky, youll be notified that you won 4 tickets to coachella, and you'll need to call back in 10 minute!



