#!/bin/bash
# Purpose:  Records a stream, logs it, then sends the file to s3 bucket
S3_BUCKET=kson-mp3-bucket
RECORD_LENGTH=180 # length (seconds), 5 min at which the recording will go for
DIR=~/kson-radio-bot/recordings
DATE=$(/bin/date "+%FT%T") #get date stamp for file
OUTPUT=KSONFM.mp3 #append to file time stamp 
LOGLOCATION=~/kson-radio-bot/kson_bot.log
FILENAME=$(echo $DATE-$OUTPUT)

# specify region 
export AWS_DEFAULT_REGION=us-west-2

echo $FILENAME
# logging start  
printf "\n$(/bin/date "+%FT%T") ... Starting Recording; planning to run for $RECORD_LENGTH seconds ( $(($RECORD_LENGTH / 60)) minute(s) )" >> $LOGLOCATION

#run VLC, redirect stream to mp3 file
/usr/bin/cvlc -vvv https://playerservices.streamtheworld.com/api/livestream-redirect/KSONFM.mp3 --sout=file/mp3:$DIR/$DATE-$OUTPUT --run-time $RECORD_LENGTH vlc://quit

# logging finish 
printf "\n$(/bin/date "+%FT%T") ... Ending Recording" >> $LOGLOCATION

# copy file to s3
printf "\n$(/bin/date "+%FT%T") ... Sending file \"$DATE-$OUTPUT\" to S3bucket named: $S3_BUCKET \n" >> $LOGLOCATION
/bin/aws s3 cp $DIR/$FILENAME s3://$S3_BUCKET/$FILENAME


# transcribe file via python script 
printf "\n$(/bin/date "+%FT%T") ... Preparing to Transcribe file: $FILENAME"
/usr/bin/python ~/kson-radio-bot/00-start_transcribe_job.py $FILENAME


# pull down the transcript text
export AWS_DEFAULT_REGION=us-west-2
JOBNAME=$(echo $FILENAME | sed 's/\:/./g') #giving proper format to the jobname
printf "\n$(/bin/date "+%FT%T") ... Pulling down transcript from job: \"$JOBNAME\""
curl $(/bin/aws transcribe \
    get-transcription-job --transcription-job-name $JOBNAME | \
    grep TranscriptFileUri | awk '{print $2}' | sed 's/\"//g') | \
    jq -r '.[]' | grep 'transcript": ' > ~/kson-radio-bot/transcripts/$DATE-KSONFM.txt

# place the trascript into S3 bucket
/bin/aws s3 cp ~/kson-radio-bot/transcripts/$DATE-KSONFM.txt s3://$S3_BUCKET/$DATE-KSONFM.txt
