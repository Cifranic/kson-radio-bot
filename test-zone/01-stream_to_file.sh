#!/bin/bash
# Purpose:  Records a stream, logs it, then sends the file to s3 bucket
S3_BUCKET=mp3_radio_dumps
RECORD_LENGTH=60 # length (seconds), 5 min at which the recording will go for
DIR=~/kson-radio-bot/recordings
DATE=$(date "+%FT%T") #get date stamp for file
OUTPUT=KSONFM.mp3 #append to file time stamp 
LOGLOCATION=~/kson-radio-bot/kson_bot.log

# logging start  
printf "\n$(date "+%FT%T") ... Starting Recording; planning to run for $RECORD_LENGTH seconds ( $(($RECORD_LENGTH / 60)) minute(s) )" >> $LOGLOCATION

#run VLC, redirect stream to mp3 file
/usr/bin/cvlc -vvv https://playerservices.streamtheworld.com/api/livestream-redirect/KSONFM.mp3 --sout=file/mp3:$DIR/$DATE-$OUTPUT --run-time $RECORD_LENGTH vlc://quit

# logging finish 
printf "\n$(date "+%FT%T") ... Ending Recording" >> $LOGLOCATION

# send to s3
printf "\n$(date "+%FT%T") ... Sending file \"$DATE-$OUTPUT\" to S3bucket named: $S3_BUCKET \n" >> $LOGLOCATION
