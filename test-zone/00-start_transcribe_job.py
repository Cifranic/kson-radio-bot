# usage: python2 <bucketname> <filename>
from __future__ import print_function
import time
import boto3
transcribe = boto3.client('transcribe')
job_name =  "2019-12-12T09:38:01-KSONFM.mp3"
bucketname = "kson-mp3-bucket"
region = "us-west-2"
job_uri = "https://" + str(bucketname) + ".s3-" + str(region) + ".amazonaws.com/" + str(job_name)

print(job_uri)
#transcribe.start_transcription_job(
#    TranscriptionJobName=job_name,
#    Media={'MediaFileUri': job_uri},
#    MediaFormat='wav',
#    LanguageCode='en-US'
#)
#while True:
#    status = transcribe.get_transcription_job(TranscriptionJobName=job_name)
#    if status['TranscriptionJob']['TranscriptionJobStatus'] in ['COMPLETED', 'FAILED']:
#        break
#    print("Not ready yet...")
#    time.sleep(5)
#print(status)
