import boto3

# A function to send a text message of a KSON script
# runtime: python 2.7

def lambda_handler(event, context):

    s3 = boto3.resource('s3')
    bucket=s3.Bucket('kson-mp3-bucket')
    
    ## get the most recently uploaded txt transcipt file, and save it into variable "last_txt_transcript"
    for key in bucket.objects.all():
            # itterate thorugh list of ALL txt files in the s3 bucket
            if key.key.endswith('.txt'):
                    # prints all txt files 
                    #print(key.key)
    
                    # gets the last txt file in the list, organized by date
                    last_txt_transcript = key.key
    
    print("\n\nThe last txt file is: " + last_txt_transcript + "\n\n")
    
    ## save the contents of txt transcript file in variable "body"
    bucket_name='kson-mp3-bucket'
    obj = s3.Object(bucket_name, last_txt_transcript)
    body = obj.get()['Body'].read()
    print(body)
    
    ## injest file with contestant names, save them to an array
     
    ## check the body of transcipt for keynames from array (ex: Nicholas nicholas nick, jenna, jena, brink, cifranic)
    
    ## send SMS text message 
    sns = boto3.client('sns')
    number = '+15555551234'
    #sns.publish(PhoneNumber = number, Message='example text message' )
    sns.publish(PhoneNumber = number, Message=body )

