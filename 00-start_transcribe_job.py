# usage: python2 <filename>
from __future__ import print_function
import time
import boto3
import sys
transcribe = boto3.client('transcribe', region_name='us-west-2')
file_name = sys.argv[1]
#file_name = "2019-12-11T12:38:01-KSONFM.mp3"
job_name = file_name.replace(":", ".")
bucketname = "kson-mp3-bucket"
region = "us-west-2"
job_uri = "https://" + str(bucketname) + ".s3-" + str(region) + ".amazonaws.com/" + str(file_name)

print(job_uri)
transcribe.start_transcription_job(
    TranscriptionJobName=job_name,
    Media={'MediaFileUri': job_uri},
    MediaFormat='mp3',
    LanguageCode='en-US'
)
while True:
    status = transcribe.get_transcription_job(TranscriptionJobName=job_name)
    if status['TranscriptionJob']['TranscriptionJobStatus'] in ['COMPLETED', 'FAILED']:
        break
    print("Not ready yet...")
    time.sleep(5)
print(status)
